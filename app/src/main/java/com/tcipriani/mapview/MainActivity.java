package com.tcipriani.mapview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;

import co.moonmonkeylabs.realmsfrestaurantdata.SFRestaurantDataLoader;
import co.moonmonkeylabs.realmsfrestaurantdata.SFRestaurantModule;
import co.moonmonkeylabs.realmsfrestaurantdata.model.Business;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Sets default realm with sample data module
        Realm.setDefaultConfiguration(new RealmConfiguration
                .Builder(this)
                .setModules(Realm.getDefaultModule(), new SFRestaurantModule())
                .build());
        // Loads and adds sample data to realm
        try {

            ArrayList businesses = new ArrayList();

            businesses.add(new Business(1,"Makro - CIT",-22.8113573f,-47.0480149f));
            businesses.add(new Business(2,"Makro pego a mina",-22.8157233f,-47.0445993f));
            businesses.add(new Business(3,"Makro 2",-22.8157233f,-47.0445993f));
            businesses.add(new Business(4,"Makro fittipaldi ",-22.8157233f,-47.0445993f));
            businesses.add(new Business(5,"Makro nishida",-22.8179699f,-47.0455475f));
            businesses.add(new Business(6,"Makro stag",-22.8179459f,-47.0634951f));
            businesses.add(new Business(7,"Makro matheus",-22.8328675f,-47.0517969f));
            businesses.add(new Business(8,"Makro Tiago",-22.8414632f,-47.0477223f));
            businesses.add(new Business(9,"Makro celega",-22.8192366f,-47.0641955f));


            Realm var19 = Realm.getDefaultInstance();
            var19.beginTransaction();
            var19.copyToRealm(businesses);
            var19.commitTransaction();
            var19.close();

        }catch (Exception ex){

        }
        // Sets layout with map fragment
        setContentView(R.layout.activity_main);
    }
}
