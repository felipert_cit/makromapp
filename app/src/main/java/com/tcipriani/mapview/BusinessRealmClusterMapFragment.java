package com.tcipriani.mapview;

import co.moonmonkeylabs.realmmapview.RealmClusterMapFragment;
import co.moonmonkeylabs.realmsfrestaurantdata.model.Business;

public class BusinessRealmClusterMapFragment extends RealmClusterMapFragment<Business> {


    @Override
    protected String getTitleColumnName() {
        return "name";
    }

    @Override
    protected String getLatitudeColumnName() {
        return "latitude";
    }

    @Override
    protected String getLongitudeColumnName() {
        return "longitude";
    }
}
